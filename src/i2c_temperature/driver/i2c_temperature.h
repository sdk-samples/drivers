// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_I2C_TEMPERATURE_H_
#define FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_I2C_TEMPERATURE_H_

#include <fidl/examples.i2c.temperature/cpp/wire.h>
#include <lib/driver/component/cpp/driver_base.h>
#include <lib/driver/devfs/cpp/connector.h>

#include "i2c_channel.h"

namespace i2c_temperature {

// Sample driver for a virtual temperature sensor device connected to an I2C bus controller.
class I2cTemperatureDriver : public fdf::DriverBase {
 public:
  I2cTemperatureDriver(fdf::DriverStartArgs start_args,
                       fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("i2c-temperature", std::move(start_args), std::move(driver_dispatcher)),
        devfs_connector_(fit::bind_member<&I2cTemperatureDriver::Serve>(this)) {}

  virtual ~I2cTemperatureDriver() = default;

  zx::result<> Start() override;

 private:
  zx::result<> SetupI2cChannel();
  zx::result<> ExportToDevfs();
  void Serve(fidl::ServerEnd<examples_i2c_temperature::Device> request);

  fidl::WireSyncClient<fuchsia_driver_framework::Node> node_;
  fidl::WireSyncClient<fuchsia_driver_framework::NodeController> controller_;
  driver_devfs::Connector<examples_i2c_temperature::Device> devfs_connector_;

  std::shared_ptr<I2cChannel> i2c_channel_;
};

}  // namespace i2c_temperature

#endif  // FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_I2C_TEMPERATURE_H_
