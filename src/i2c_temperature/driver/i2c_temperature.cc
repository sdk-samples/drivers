// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "i2c_temperature.h"

#include <lib/driver/component/cpp/driver_export.h>

#include "constants.h"
#include "temperature_server.h"

namespace i2c_temperature {

zx::result<> I2cTemperatureDriver::Start() {
  // Connect to the I2C bus controller.
  auto channel_result = SetupI2cChannel();
  if (channel_result.is_error()) {
    return channel_result.take_error();
  }

  // Serve the examples.i2c.temperature/Device protocol to clients through the
  // examples.i2c.temperature/Service wrapper.
  examples_i2c_temperature::Service::InstanceHandler handler({
      .device = fit::bind_member<&I2cTemperatureDriver::Serve>(this),
  });

  auto result = outgoing()->AddService<examples_i2c_temperature::Service>(std::move(handler));
  if (result.is_error()) {
    FDF_SLOG(ERROR, "Failed to add service", KV("status", result.status_string()));
    return result.take_error();
  }

  // Reset the sensor.
  auto write_result = i2c_channel_->Write16(kSoftResetCommand);
  if (write_result.is_error()) {
    FDF_SLOG(ERROR, "Failed to send reset command: ", KV("status", write_result.status_string()));
    return write_result.take_error();
  }

  if (zx::result result = ExportToDevfs(); result.is_error()) {
    FDF_SLOG(ERROR, "Failed to export to devfs", KV("status", result.status_string()));
    return result.take_error();
  }

  return zx::ok();
}

// Connect to the `fuchsia.hardware.i2c/Device` protocol offered by the parent device node.
zx::result<> I2cTemperatureDriver::SetupI2cChannel() {
  zx::result client_endpoint =
      incoming()->Connect<fuchsia_hardware_i2c::Service::Device>("default");
  if (client_endpoint.status_value() != ZX_OK) {
    FDF_SLOG(ERROR, "Failed to setup I2cChannel", KV("status", client_endpoint.status_string()));
    return client_endpoint.take_error();
  }

  auto i2c_client = fidl::WireClient(std::move(*client_endpoint), dispatcher());
  i2c_channel_ = std::make_shared<I2cChannel>(std::move(i2c_client));
  return zx::ok();
}

zx::result<> I2cTemperatureDriver::ExportToDevfs() {
  // Create a node for devfs.
  fidl::Arena arena;
  zx::result connector = devfs_connector_.Bind(dispatcher());
  if (connector.is_error()) {
    return connector.take_error();
  }

  auto devfs = fuchsia_driver_framework::wire::DevfsAddArgs::Builder(arena).connector(
      std::move(connector.value()));

  auto args = fuchsia_driver_framework::wire::NodeAddArgs::Builder(arena)
                  .name(arena, name())
                  .devfs_args(devfs.Build())
                  .Build();

  // Create endpoints of the `NodeController` for the node.
  zx::result controller_endpoints =
      fidl::CreateEndpoints<fuchsia_driver_framework::NodeController>();
  ZX_ASSERT_MSG(controller_endpoints.is_ok(), "Failed: %s", controller_endpoints.status_string());

  zx::result node_endpoints = fidl::CreateEndpoints<fuchsia_driver_framework::Node>();
  ZX_ASSERT_MSG(node_endpoints.is_ok(), "Failed: %s", node_endpoints.status_string());

  fidl::WireResult result = fidl::WireCall(node())->AddChild(
      args, std::move(controller_endpoints->server), std::move(node_endpoints->server));
  if (!result.ok()) {
    FDF_SLOG(ERROR, "Failed to add child", KV("status", result.status_string()));
    return zx::error(result.status());
  }
  controller_.Bind(std::move(controller_endpoints->client));
  node_.Bind(std::move(node_endpoints->client));
  return zx::ok();
}

void I2cTemperatureDriver::Serve(fidl::ServerEnd<examples_i2c_temperature::Device> request) {
  I2cTemperatureServer::BindDeviceClient(dispatcher(), i2c_channel_, std::move(request));
}

}  // namespace i2c_temperature

FUCHSIA_DRIVER_EXPORT(i2c_temperature::I2cTemperatureDriver);
