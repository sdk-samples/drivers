// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_I2C_CHANNEL_H_
#define FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_I2C_CHANNEL_H_

#include <fidl/fuchsia.hardware.i2c/cpp/wire.h>

namespace i2c_temperature {

// Sends read and write transactions through a fuchsia.hardware.i2c client.
class I2cChannel {
 public:
  explicit I2cChannel(fidl::WireClient<fuchsia_hardware_i2c::Device> client_end)
      : fidl_client_(std::move(client_end)) {}

  I2cChannel(I2cChannel&& other) noexcept = default;
  I2cChannel& operator=(I2cChannel&& other) noexcept = default;

  ~I2cChannel() = default;

  // Functions that perform read/write transactions through the client.
  // The values are transferred across the I2C channel in big-endian order.
  zx::result<uint16_t> Read16();
  zx::result<> Write16(uint16_t value);

 private:
  zx::result<> WriteReadSync(const uint8_t* tx_buf, size_t tx_len, uint8_t* rx_buf, size_t rx_len);

  fidl::WireClient<fuchsia_hardware_i2c::Device> fidl_client_;
};

}  // namespace i2c_temperature

#endif  // FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_I2C_CHANNEL_H_
