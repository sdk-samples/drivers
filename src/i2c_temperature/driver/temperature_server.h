// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_TEMPERATURE_SERVER_H_
#define FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_TEMPERATURE_SERVER_H_

#include <fidl/examples.i2c.temperature/cpp/wire.h>

#include "i2c_channel.h"

namespace i2c_temperature {

// FIDL server implementation for the `examples.i2c.temperature/Device` protocol
class I2cTemperatureServer : public fidl::WireServer<examples_i2c_temperature::Device> {
 public:
  I2cTemperatureServer(std::weak_ptr<I2cChannel> i2c_channel) : i2c_channel_(i2c_channel) {}

  static fidl::ServerBindingRef<examples_i2c_temperature::Device> BindDeviceClient(
      async_dispatcher_t* dispatcher, std::weak_ptr<I2cChannel> i2c_channel,
      fidl::ServerEnd<examples_i2c_temperature::Device> request);

  void OnUnbound(fidl::UnbindInfo info,
                 fidl::ServerEnd<examples_i2c_temperature::Device> server_end);

  // fidl::WireServer<examples_i2c_temperature::Device>

  void ReadTemperature(ReadTemperatureCompleter::Sync& completer) override;

  void ResetSensor(ResetSensorCompleter::Sync& completer) override;

 private:
  std::weak_ptr<I2cChannel> i2c_channel_;
};

}  // namespace i2c_temperature

#endif  // FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_DRIVER_TEMPERATURE_SERVER_H_
