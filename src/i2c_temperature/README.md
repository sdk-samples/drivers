# I2C Temperature Sample

This sample project contains a driver component named `i2c_temperature` for a temperature
sensor device connected to an I2C bus with device address `0xFF`.
The driver interacts with the device hardware using the `fuchsia.hardware.i2c` protocol,
and exposes a custom FIDL protocol (`examples.i2c.temperature`) for other components
to consume.

The `i2c_controller` driver component emulates an I2C bus controller, creating the child
device nodes to the sensor device and responding to I2C protocol commands received from
the child devices.

## Building

To build the `i2c_temperature` driver and related components, run the following commands:

```
tools/bazel build --config=fuchsia_x64 //src/i2c_temperature/controller:pkg
tools/bazel build --config=fuchsia_x64 //src/i2c_temperature/driver:pkg
```

## Running

Use the following commands to load the driver components on a target device:

1.  Add a test node for the `i2c_controller` driver:

    ```
    tools/ffx driver test-node add controller-node \
      examples.driver.test.property=i2c-temperature-controller
    ```

1.  Load the `i2c_controller` driver component to create a virtual I2C device node:

    ```
    tools/bazel run //src/i2c_temperature/controller:pkg.component
    ```

1.  Load the `i2c_temperature` driver component to bind to the device node and begin
    sending requests:

    ```
    tools/bazel run //src/i2c_temperature/driver:pkg.component
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --tags i2c-temperature --tags i2c-temperature-controller
    ```

You should see the `i2c_controller` driver log the requests it receives from `i2c_temperature`
after the driver has successfully bound.

```
[i2c-temperature-controller,driver][I]: [i2c_controller.cc:139] Received transfer request
[i2c-temperature-controller,driver][I]: [i2c_controller.cc:126] Reset command received
```

### Driver client

After the drivers are loaded, use the following commands to run the temperature client:

1.  Load the `temperature_client` component:

    ```
    tools/bazel run --config=fuchsia_x64 //src/i2c_temperature/client:pkg.component
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --filter temperature_client
    ```

You should see the client respond with an incrementing temperature value on each run:

```
[ffx-laboratory:temperature_client][I] Current temperature: 25.000000
```

## Source layout

*   `controller/` — Source code of the `i2c_controller` driver component.
*   `driver/` — Source code of the `i2c_temperature` driver component.
*   `fidl/` — FIDL library definition for `examples.i2c.temperature`.
*   `lib/` — Common I2C constants shared between the controller and driver components.
