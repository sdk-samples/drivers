// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_CONTROLLER_I2C_SERVER_H_
#define FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_CONTROLLER_I2C_SERVER_H_

#include <fidl/fuchsia.hardware.i2c/cpp/wire.h>

namespace i2c_temperature {

// FIDL server implementation for the `fuchsia.hardware.i2c/Device` protocol
class I2cDeviceServer : public fidl::WireServer<fuchsia_hardware_i2c::Device> {
  static constexpr uint32_t kStartingTemp = 20;
  static constexpr uint32_t kTempIncrement = 5;

 public:
  explicit I2cDeviceServer() : temperature_(kStartingTemp) {}

  static fidl::ServerBindingRef<fuchsia_hardware_i2c::Device> BindDeviceClient(
      std::shared_ptr<I2cDeviceServer> server_impl, async_dispatcher_t* dispatcher,
      fidl::ServerEnd<fuchsia_hardware_i2c::Device> request);
  void OnUnbound(fidl::UnbindInfo info, fidl::ServerEnd<fuchsia_hardware_i2c::Device> server_end);

  // fidl::WireServer<fuchsia_hardware_i2c::Device>

  void Transfer(TransferRequestView request, TransferCompleter::Sync& completer) override;
  void GetName(GetNameCompleter::Sync& completer) override;

 private:
  void HandleWrite(const fidl::VectorView<uint8_t> write_data);
  uint32_t temperature_;
};

}  // namespace i2c_temperature

#endif  // FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_CONTROLLER_I2C_SERVER_H_
