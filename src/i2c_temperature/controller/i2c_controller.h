// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_CONTROLLER_I2C_CONTROLLER_H_
#define FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_CONTROLLER_I2C_CONTROLLER_H_

#include <lib/driver/component/cpp/driver_base.h>

#include "i2c_server.h"

namespace i2c_temperature {

// Sample driver that implements a `fuchsia.hardware.i2c` bus controller and emulates the
// behavior of a temperature sensor device to interact with the i2c temperature driver.
class I2cTemperatureController : public fdf::DriverBase {
 public:
  I2cTemperatureController(fdf::DriverStartArgs start_args,
                           fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("i2c-temperature-controller", std::move(start_args),
                        std::move(driver_dispatcher)) {}

  virtual ~I2cTemperatureController() = default;

  zx::result<> Start() override;

 private:
  zx::result<> AddChild();

  fidl::WireSharedClient<fuchsia_driver_framework::Node> node_;
  fidl::WireSharedClient<fuchsia_driver_framework::NodeController> controller_;
  std::shared_ptr<I2cDeviceServer> i2c_server_;
};

}  // namespace i2c_temperature

#endif  // FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_CONTROLLER_I2C_CONTROLLER_H_
