// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_LIB_INCLUDE_CONSTANTS_H_
#define FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_LIB_INCLUDE_CONSTANTS_H_

namespace i2c_temperature {

// Command values sent to the I2C Device.
constexpr uint16_t kSoftResetCommand = 0x805d;
constexpr uint16_t kStartMeasurementCommand = 0x7866;

}  // namespace i2c_temperature

#endif  // FUCHSIA_SDK_EXAMPLES_I2C_TEMPERATURE_LIB_INCLUDE_CONSTANTS_H_
