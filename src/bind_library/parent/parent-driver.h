// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_PARENT_PARENT_DRIVER_H_
#define FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_PARENT_PARENT_DRIVER_H_

#include <lib/driver/component/cpp/driver_export.h>

#include "gizmo_server.h"

namespace parent_driver {

class ParentDriver : public fdf::DriverBase {
 public:
  ParentDriver(fdf::DriverStartArgs start_args,
               fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("parent-driver", std::move(start_args), std::move(driver_dispatcher)) {}
  virtual ~ParentDriver() = default;

  zx::result<> Start() override;

 private:
  zx::result<> AddChild();

  fidl::WireClient<fuchsia_driver_framework::Node> node_;
  std::shared_ptr<GizmoServer> gizmo_server_;
};

}  // namespace parent_driver

#endif  // FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_PARENT_PARENT_DRIVER_H_
