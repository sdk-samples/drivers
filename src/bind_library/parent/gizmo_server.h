// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_PARENT_GIZMO_SERVER_H_
#define FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_PARENT_GIZMO_SERVER_H_

#include <fidl/examples.gizmo/cpp/wire.h>
#include <lib/driver/logging/cpp/structured_logger.h>

namespace parent_driver {

class GizmoServer : public fidl::WireServer<examples_gizmo::TestingProtocol> {
 public:
  GizmoServer() {}
  virtual ~GizmoServer() = default;

  static fidl::ServerBindingRef<examples_gizmo::TestingProtocol> BindDeviceClient(
      std::shared_ptr<GizmoServer> server_impl, async_dispatcher_t* dispatcher,
      fidl::ServerEnd<examples_gizmo::TestingProtocol> request) {
    // Bind each connection request to the shared instance.
    return fidl::BindServer(dispatcher, std::move(request), server_impl,
                            std::mem_fn(&GizmoServer::OnUnbound));
  }

  void OnUnbound(fidl::UnbindInfo info,
                 fidl::ServerEnd<examples_gizmo::TestingProtocol> server_end) {
    if (info.is_peer_closed()) {
      FDF_LOG(DEBUG, "Client disconnected");
    } else if (!info.is_user_initiated()) {
      FDF_LOG(ERROR, "Client connection unbound: %s", info.status_string());
    }
  }

  // fidl::WireServer<examples_gizmo::TestingProtocol>

  void Get(GetCompleter::Sync& completer) {
    FDF_LOG(INFO, "Received TestingProtocol.Get request.");
  }
};

}  // namespace parent_driver

#endif  // FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_PARENT_GIZMO_SERVER_H_
