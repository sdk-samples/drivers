# Bind library code generation sample

This project is a sample of the bind library code generation feature. It contaains
two drivers, a parent and child.

The parent driver creates a child node with some properties from the bind library's generated
C++ library to showcase its usage in code.

The child driver will bind to this node by specifying in its bind rules
the node properties the parent driver has specified.

## Building

To build the driver components, run the following commands:

```
tools/bazel build --config=fuchsia_x64 //src/bind_library/parent:pkg
tools/bazel build --config=fuchsia_x64 //src/bind_library/child:pkg
```

## Running

Use the following commands to load the driver components on a target device:

1.  Load the parent driver component:

    ```
    tools/bazel run --config=fuchsia_x64 //src/bind_library/parent:pkg.component
    ```

1.  Run the child driver component:

    ```
    tools/bazel run --config=fuchsia_x64 //src/bind_library/child:pkg.component
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --tags driver --filter universe-pkg-drivers
    ```

You should see the following logs:

```
[gizmo_server.h:38] Received TestingProtocol.Get request.
[child-driver.cc:29] Succeeded!
```

## Source layout

*   `parent/` — Source code of the parent driver component.
*   `child/` — Source code of the child driver component.
*   `lib/` — FIDL and bind libraries shared between the two components.
