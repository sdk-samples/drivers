// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_CHILD_CHILD_DRIVER_H_
#define FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_CHILD_CHILD_DRIVER_H_

#include <lib/driver/component/cpp/driver_base.h>

namespace child_driver {

class ChildDriver : public fdf::DriverBase {
 public:
  ChildDriver(fdf::DriverStartArgs start_args, fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("child-driver", std::move(start_args), std::move(driver_dispatcher)) {}
  virtual ~ChildDriver() = default;

  zx::result<> Start() override;
};

}  // namespace child_driver

#endif  // FUCHSIA_SDK_EXAMPLES_BIND_LIBRARY_CHILD_CHILD_DRIVER_H_
