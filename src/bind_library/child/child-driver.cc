// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "child-driver.h"

#include <fidl/examples.gizmo/cpp/wire.h>
#include <lib/driver/component/cpp/driver_export.h>
#include <lib/driver/component/cpp/service_client.h>

namespace child_driver {

zx::result<> ChildDriver::Start() {
  auto connect_result = incoming()->Connect<examples_gizmo::Service::Testing>();
  if (connect_result.is_error()) {
    FDF_SLOG(WARNING, "Failed to connect to gizmo service.",
             KV("status", connect_result.status_string()));
    return connect_result.take_error();
  }

  auto client = fidl::WireSyncClient(std::move(connect_result.value()));
  auto get_result = client->Get();
  if (!get_result.ok()) {
    FDF_SLOG(WARNING, "Failed to send Get to TestingProtocol.",
             KV("status", get_result.error().status_string()));
    return zx::error(get_result.error().status());
  }

  FDF_LOG(INFO, "Succeeded!");
  return zx::ok();
}

}  // namespace child_driver

FUCHSIA_DRIVER_RECORD_CPP_V2(fdf::Record<child_driver::ChildDriver>);
