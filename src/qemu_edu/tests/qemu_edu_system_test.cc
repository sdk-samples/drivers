// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// [START imports]
#include <dirent.h>
#include <fcntl.h>
#include <fidl/examples.qemuedu/cpp/wire.h>
#include <lib/fdio/directory.h>
#include <sys/types.h>

#include <filesystem>

#include <gtest/gtest.h>
// [END imports]

// [START main_body]
namespace {

constexpr char kDevfsRootPath[] = "/dev/sys/platform/";
constexpr char kEduDevicePath[] = "qemu-edu";

class QemuEduSystemTest : public testing::Test {
 public:
  void SetUp() {
    auto device_path = SearchDevicePath();
    ASSERT_TRUE(device_path.has_value());
    zx::result endpoints = fidl::CreateEndpoints<examples_qemuedu::Device>();
    ASSERT_EQ(endpoints.status_value(), ZX_OK);
    ASSERT_EQ(fdio_service_connect(device_path.value().c_str(),
                                   endpoints->server.TakeChannel().release()),
              ZX_OK);
    device_ = fidl::WireSyncClient(std::move(endpoints->client));
  }

  // [START_EXCLUDE silent]
  // TODO(fxbug.dev/114888): Remove this once we can alias a stable path for generic devices.
  // [END_EXCLUDE]
  // Search for the device file entry in devfs
  std::optional<std::string> SearchDevicePath() {
    for (auto const& dir_entry : std::filesystem::recursive_directory_iterator(kDevfsRootPath)) {
      if (dir_entry.path().string().find(kEduDevicePath) != std::string::npos) {
        return {dir_entry.path()};
      }
    }

    return {};
  }

  fidl::WireSyncClient<examples_qemuedu::Device>& device() { return device_; }

 private:
  fidl::WireSyncClient<examples_qemuedu::Device> device_;
};

TEST_F(QemuEduSystemTest, LivenessCheck) {
  fidl::WireResult result = device()->LivenessCheck();
  ASSERT_EQ(result.status(), ZX_OK);
  ASSERT_TRUE(result->value()->result);
}

TEST_F(QemuEduSystemTest, ComputeFactorial) {
  std::array<uint32_t, 11> kExpected = {
      1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800,
  };
  for (uint32_t i = 0; i < kExpected.size(); i++) {
    fidl::WireResult result = device()->ComputeFactorial(i);
    ASSERT_EQ(result.status(), ZX_OK);
    EXPECT_EQ(result->value()->output, kExpected[i]);
  }
}

}  // namespace
// [END main_body]
