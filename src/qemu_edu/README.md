# QEMU edu driver sample

This sample project contains a Fuchsia driver for the QEMU `edu` device, a virtual PCI
device built into the Fuchsia emulator (based on QEMU) that computes factorial values.
The driver interacts with the device hardware using the `fuchsia.hardware.pci` protocol,
and exposes a custom FIDL protocol (`examples.qemuedu`) for other components to
consume.

## Building

To build the `qemu_edu` driver and related components, run the following command:

```
tools/bazel build --config=fuchsia_x64 //src/qemu_edu/drivers:pkg
tools/bazel build --config=fuchsia_x64 //src/qemu_edu/tests:pkg
tools/bazel build --config=fuchsia_x64 //src/qemu_edu/tools:pkg
```

## Running

Use the following commands to load the driver and run the command line tool on a target
device:

1.  Load the `qemu_edu` driver component:

    ```
    tools/bazel run --config=fuchsia_x64 //src/qemu_edu/drivers:pkg.component
    ```

1.  Run the `eductl` tools component, pass the arguments `fact 12` to compute a factorial:

    ```
    tools/bazel run --config=fuchsia_x64 //src/qemu_edu/tools:pkg.eductl_tool -- fact 12
    ```

    You should see the tool print the factorial result:
    
    ```
    Factorial(12) = 479001600
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --tags qemu-edu
    ```

    You should see log messages printed by the driver as it loads and responds to requests:

    ```
    [qemu-edu,driver][I]: [src/qemu_edu/drivers/qemu_edu.cc:179] edu device version major=1 minor=0
    [qemu-edu,driver][I]: [src/qemu_edu/drivers/edu_device.cc:124] Replying with factorial=479001600
    ```

## Testing

Use the following command to run the system test component for the `qemu_edu` driver on a
target device:

```
tools/bazel run --config=fuchsia_x64 //src/qemu_edu/tests:pkg.component
```

You should see the test cases execute and pass:

```
Running test 'fuchsia-pkg://bazel.test.pkg.system.test.component/qemu_edu_system_test#meta/qemu_edu_system_test.cm'
[RUNNING]   main
[stdout - main]
Running main() from gmock_main.cc
[==========] Running 2 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 2 tests from QemuEduSystemTest
[ RUN      ] QemuEduSystemTest.LivenessCheck
[       OK ] QemuEduSystemTest.LivenessCheck (4 ms)
[ RUN      ] QemuEduSystemTest.ComputeFactorial
[       OK ] QemuEduSystemTest.ComputeFactorial (4 ms)
[----------] 2 tests from QemuEduSystemTest (9 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test suite ran. (9 ms total)
[  PASSED  ] 2 tests.
[PASSED]    main
```

## Source layout

*   `driver/` — Source code of the `qemu_edu` PCI device driver.
*   `fidl/` — FIDL library definition for `examples.qemuedu`.
*   `test/` — System test component that exercises the `qemu_edu` driver.
*   `tools/` — Command line tool (`eductl`) that interacts with the `qemu_edu` driver.
