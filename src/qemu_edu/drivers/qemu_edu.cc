// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// [START imports]
#include "qemu_edu.h"

#include <lib/driver/component/cpp/driver_export.h>
// [END imports]

// [START fidl_imports]
#include "edu_server.h"
// [END fidl_imports]

// [START namespace_start]
namespace qemu_edu {
// [END namespace_start]

// [START start_method_start]
// Initialize this driver instance
zx::result<> QemuEduDriver::Start() {
  // [END start_method_start]
  // [START connect_device]
  // Connect to the parent device node.
  zx::result connect_result = incoming()->Connect<fuchsia_hardware_pci::Service::Device>("default");
  if (connect_result.is_error()) {
    FDF_SLOG(ERROR, "Failed to open pci service.", KV("status", connect_result.status_string()));
    return connect_result.take_error();
  }
  // [END connect_device]

  // [START hw_resources]
  // Map hardware resources from the PCI device
  device_ =
      std::make_shared<edu_device::QemuEduDevice>(dispatcher(), std::move(connect_result.value()));
  auto pci_status = device_->MapInterruptAndMmio();
  if (pci_status.is_error()) {
    return pci_status.take_error();
  }
  // [END hw_resources]

  // [START device_registers]
  // Report the version information from the edu device.
  auto version_reg = device_->IdentificationRegister();
  FDF_SLOG(INFO, "edu device version", KV("major", version_reg.major_version()),
           KV("minor", version_reg.minor_version()));
  // [END device_registers]

  // [START serve_outgoing]
  // Serve the examples.qemuedu/Service capability.
  examples_qemuedu::Service::InstanceHandler handler({
      .device = fit::bind_member<&QemuEduDriver::Serve>(this),
  });

  auto add_result = outgoing()->AddService<examples_qemuedu::Service>(std::move(handler));
  if (add_result.is_error()) {
    FDF_SLOG(ERROR, "Failed to add Device service", KV("status", add_result.status_string()));
    return add_result.take_error();
  }
  // [END serve_outgoing]

  // [START devfs_export]
  if (zx::result result = ExportToDevfs(); result.is_error()) {
    FDF_SLOG(ERROR, "Failed to export to devfs", KV("status", result.status_string()));
    return result.take_error();
  }
  // Create and export a devfs entry for the driver service.
  // [END devfs_export]

  // [START start_method_end]
  return zx::ok();
}
// [END start_method_end]

zx::result<> QemuEduDriver::ExportToDevfs() {
  // Create a node for devfs.
  fidl::Arena arena;
  zx::result connector = devfs_connector_.Bind(dispatcher());
  if (connector.is_error()) {
    return connector.take_error();
  }

  auto devfs = fuchsia_driver_framework::wire::DevfsAddArgs::Builder(arena).connector(
      std::move(connector.value()));

  auto args = fuchsia_driver_framework::wire::NodeAddArgs::Builder(arena)
                  .name(arena, name())
                  .devfs_args(devfs.Build())
                  .Build();

  // Create endpoints of the `NodeController` for the node.
  zx::result controller_endpoints =
      fidl::CreateEndpoints<fuchsia_driver_framework::NodeController>();
  ZX_ASSERT_MSG(controller_endpoints.is_ok(), "Failed: %s", controller_endpoints.status_string());

  zx::result node_endpoints = fidl::CreateEndpoints<fuchsia_driver_framework::Node>();
  ZX_ASSERT_MSG(node_endpoints.is_ok(), "Failed: %s", node_endpoints.status_string());

  fidl::WireResult result = fidl::WireCall(node())->AddChild(
      args, std::move(controller_endpoints->server), std::move(node_endpoints->server));
  if (!result.ok()) {
    FDF_SLOG(ERROR, "Failed to add child", KV("status", result.status_string()));
    return zx::error(result.status());
  }
  controller_.Bind(std::move(controller_endpoints->client));
  node_.Bind(std::move(node_endpoints->client));
  return zx::ok();
}

void QemuEduDriver::Serve(fidl::ServerEnd<examples_qemuedu::Device> request) {
  QemuEduServer::BindDeviceClient(dispatcher(), device_, std::move(request));
}

// [START namespace_end]
}  // namespace qemu_edu
// [END namespace_end]

// [START driver_hook]
// Register driver hooks with the framework
FUCHSIA_DRIVER_EXPORT(qemu_edu::QemuEduDriver);
// [END driver_hook]
