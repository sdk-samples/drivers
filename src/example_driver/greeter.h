// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_EXAMPLE_DRIVER_GREETER_H_
#define FUCHSIA_SDK_EXAMPLES_EXAMPLE_DRIVER_GREETER_H_

#include <string>

namespace example_driver {

// Return a greeting from this driver component
std::string greeting(std::string_view name) {
  std::string greeting("Hello from ");
  return greeting.append(name);
}

}  // namespace example_driver

#endif  // FUCHSIA_SDK_EXAMPLES_EXAMPLE_DRIVER_GREETER_H_
