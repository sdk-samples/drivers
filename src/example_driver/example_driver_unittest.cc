// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <gtest/gtest.h>

#include "greeter.h"

TEST(ExampleDriverTest, GreetingTest) {
  std::string name = "example-driver";
  std::string expected = "Hello from example-driver";
  EXPECT_EQ(example_driver::greeting(name), expected);
}
