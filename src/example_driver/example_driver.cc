// Copyright 2023 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "example_driver.h"

#include <lib/driver/component/cpp/driver_export.h>
#include <lib/driver/logging/cpp/structured_logger.h>

#include "greeter.h"

namespace example_driver {

zx::result<> ExampleDriver::Start() {
  // Say hello
  FDF_SLOG(INFO, example_driver::greeting(name()).c_str());
  return zx::ok();
}

}  // namespace example_driver

FUCHSIA_DRIVER_EXPORT(example_driver::ExampleDriver);
