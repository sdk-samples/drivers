# Hello Driver Sample

This sample project contains a "Hello, World" driver component that showcases the bare
scaffolding necessary to register and load a driver component in Fuchsia.

## Building

To build the driver component, run the following command:

```
tools/bazel build --config=fuchsia_x64 //src/example_driver:pkg
```

## Running

Use the following commands to load the driver component on a target device:

1.  Load the `example_driver` component:
    ```
    tools/bazel run //src/example_driver:pkg.component
    ```

You should see the driver be registered in the logs:
```
[driver_index, driver] INFO: Registered driver successfully: fuchsia-pkg://bazel.pkg.publish.anonymous/example_driver#meta/example_driver.cm.
```

2. Create the test node the driver will bind to:
    ```
    tools/ffx driver test-node add property examples.driver.bind.TestProperty=TestDriver
    ```

3.  Open the device log viewer:

    ```
    tools/ffx log --filter example_driver
    ```

You should see the driver component say hello:

```
[driver_manager.cm][I]: [node.cc:608] Binding fuchsia-pkg://bazel.pkg.component/example_driver#meta/example_driver.cm to  acpi-FWCF-passthrough
[example-driver,driver][I]: [src/example_driver/example_driver.cc:30] Hello from example-driver
```

## Testing

Use the following command to run the unit tests on a target device:

```
tools/bazel test --config=fuchsia_x64 //src/example_driver:test_pkg
```
