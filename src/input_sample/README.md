# Input Driver Sample

This sample project contains an input driver component that implements `fuchsia.input.report`
using the `input_reports_reader` to declare a fake mouse device that sends reports every second.

## Building

To build the `input_sample` driver and related components, run the following command:

```
tools/bazel build //src/input_sample:pkg
```

## Running

Use the following commands to load the driver and inspect the input reports on a target
device:

1.  Load the `input_sample` driver component:

    ```
    tools/bazel run src/input_sample:pkg.component
    ```

1.  Read the input device descriptors using the `print-input-report` command:

    ```
    tools/ffx driver print-input-report descriptor
    ```

    The command prints a list of descriptors. The descriptor for the `input_sample` driver
    contains the following device info:

    *   `vendor_id`: 305419896 (0x12345678)
    *   `product_id`: 2271560481 (0x87654321)
    
    ```
    Descriptor from file: "class/input-report/002"
    {
      device_info: {
          vendor_id: 305419896
          product_id: 2271560481
          version: 0
      }
      mouse: {
          input: {
            movement_x: {
                range: {
                  min: -127
                  max: 127
                }
                unit: {
                  type: None
                  exponent: 0
                }
            }
            movement_y: {
                range: {
                  min: -127
                  max: 127
                }
                unit: {
                  type: None
                  exponent: 0
                }
            }
            scroll_v: None
            scroll_h: None
            buttons: [1, 2, 3]
            position_x: None
            position_y: None
          }
      }
      sensor: None
      touch: None
      keyboard: None
      consumer_control: None
    }
    ```

    You may exit this with CTRL-C.

1.  Stream the reports from the `input_sample` driver using the `print-input-report` command:

    ```
    tools/ffx driver print-input-report read
    ```

    The command prints output similar to the following for each report received:
    
    ```
    {
      event_time: 335266867991
      trace_id: 9
      report_id: None
      mouse: {
        movement_x: 29
        movement_y: 29
        scroll_v: None
        scroll_h: None
        pressed_buttons: [1, 3]
        position_x: None
        position_y: None
      }
      sensor: None
      touch: None
      keyboard: None
      consumer_control: None
    }
    ```

    You should see `movement_x` and `movement_y` increment in each report and the set of
    `pressed_buttons` change.
    
    You may exit this with CTRL-C.
