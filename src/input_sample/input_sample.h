// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_INPUT_SAMPLE_INPUT_SAMPLE_H_
#define FUCHSIA_SDK_EXAMPLES_INPUT_SAMPLE_INPUT_SAMPLE_H_

#include <lib/driver/component/cpp/driver_base.h>
#include <lib/driver/devfs/cpp/connector.h>

#include "input_server.h"

namespace input_sample {

// Sample driver for a virtual input device that generates input reports.
class InputSampleDriver : public fdf::DriverBase {
 public:
  InputSampleDriver(fdf::DriverStartArgs start_args,
                    fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("input-sample", std::move(start_args), std::move(driver_dispatcher)),
        devfs_connector_(fit::bind_member<&InputSampleDriver::Serve>(this)) {}

  virtual ~InputSampleDriver() = default;

  zx::result<> Start() override;

 private:
  void SendFakeReport();
  zx::result<> ExportToDevfs();
  void Serve(fidl::ServerEnd<fuchsia_input_report::InputDevice> request);

  driver_devfs::Connector<fuchsia_input_report::InputDevice> devfs_connector_;
  fidl::WireSyncClient<fuchsia_driver_framework::Node> node_;
  fidl::WireSyncClient<fuchsia_driver_framework::NodeController> controller_;

  std::shared_ptr<input_report_reader::InputReportReaderManager<SampleInputReport>>
      input_report_readers_;
  SampleInputReport report_;
};

}  // namespace input_sample

#endif  // FUCHSIA_SDK_EXAMPLES_INPUT_SAMPLE_INPUT_SAMPLE_H_
