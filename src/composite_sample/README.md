# Composite Device Sample

This sample project contains a Fuchsia driver for a composite device node. The driver binds with
multiple parent device nodes that the controller driver creates.

## Building

To build the `composite_sample` driver and related components, run the following command:

```
tools/bazel build //src/composite_sample/controller:pkg
tools/bazel build //src/composite_sample/driver:pkg
```

## Running

Use the following commands to load the driver component on a target device:

1.  Add a test node for the controller:

    ```
    tools/ffx driver test-node add controller-node examples.driver.test.property=controller
    ```

1.  Load the `controller` driver component:

    ```
    tools/bazel run //src/composite_sample/controller:pkg.component
    ```

1.  Open the device log viewer to confirm the test nodes were added:

    ```
    tools/ffx log --tags controller_driver
    ```

1.  Load the `composite_sample` driver component:

    ```
    tools/bazel run //src/composite_sample/driver:pkg.component
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --tags composite-sample
    ```

You should see the driver component print after the driver has successfully bound:

```
[composite-sample,driver][I]: [src/composite_sample/composite_sample.cc:10] Hello from the composite driver!
```
