# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

load(
    "@rules_fuchsia//fuchsia:defs.bzl",
    "fuchsia_cc_driver",
    "fuchsia_component_manifest",
    "fuchsia_driver_bind_bytecode",
    "fuchsia_driver_component",
    "fuchsia_package",
)

fuchsia_cc_driver(
    name = "controller_driver",
    srcs = [
        "controller.cc",
        "controller.h",
    ],
    deps = [
        "//src/testing/lib:examples.driver.test_cc",
        "@fuchsia_sdk//pkg/driver_component_cpp",
    ],
)

fuchsia_driver_bind_bytecode(
    name = "bind_bytecode",
    output = "controller-driver.bindbc",
    rules = "controller-driver.bind",
    deps = [
        "//src/testing/lib:examples.driver.test",
    ],
)

fuchsia_component_manifest(
    name = "manifest",
    src = "meta/controller-driver.cml",
    includes = [
        "@fuchsia_sdk//pkg/syslog:client",
    ],
)

fuchsia_driver_component(
    name = "component",
    bind_bytecode = ":bind_bytecode",
    driver_lib = ":controller_driver",
    manifest = ":manifest",
)

fuchsia_package(
    name = "pkg",
    package_name = "controller_pkg",
    components = [
        ":component",
    ],
    visibility = ["//visibility:public"],
)
