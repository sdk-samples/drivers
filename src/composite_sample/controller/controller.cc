// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "controller.h"

#include <lib/driver/component/cpp/driver_export.h>

#include <bind/examples/driver/test/cpp/bind.h>

namespace controller_driver {

// Name these differently than what the child expects, so we test that
// the Driver Framework renames these correctly.
const std::string_view kNodeOne = "node-one";
const std::string_view kNodeTwo = "node-two";

zx::result<> ControllerDriver::Start() {
  node_.Bind(std::move(node()), dispatcher());

  auto result = AddChild(kNodeOne, child_one_);
  if (result.status_value() != ZX_OK) {
    return result;
  }

  result = AddChild(kNodeTwo, child_two_);
  if (result.status_value() != ZX_OK) {
    return result;
  }

  FDF_LOG(INFO, "Test nodes added successfully.");

  return zx::ok();
}

zx::result<> ControllerDriver::AddChild(
    std::string_view name, fidl::WireClient<fuchsia_driver_framework::NodeController>& controller) {
  fidl::Arena arena;

  auto properties = fidl::VectorView<fuchsia_driver_framework::wire::NodeProperty>(arena, 1);
  properties[0] = fdf::MakeProperty(arena, bind_examples_driver_test::PROPERTY, name);

  auto args = fuchsia_driver_framework::wire::NodeAddArgs::Builder(arena)
                  .name(arena, name)
                  .properties(properties)
                  .Build();

  // Create endpoints of the `NodeController` for the node.
  auto endpoints = fidl::CreateEndpoints<fuchsia_driver_framework::NodeController>();
  if (endpoints.is_error()) {
    FDF_SLOG(ERROR, "Failed to create endpoint", KV("status", endpoints.status_string()));
    return zx::error(endpoints.status_value());
  }

  auto result = node_.sync()->AddChild(args, std::move(endpoints->server), {});
  if (!result.ok()) {
    FDF_SLOG(ERROR, "Failed to add child", KV("status", result.status_string()));
    return zx::error(result.status());
  }

  controller.Bind(std::move(endpoints->client), dispatcher());

  return zx::ok();
}

}  // namespace controller_driver

FUCHSIA_DRIVER_EXPORT(controller_driver::ControllerDriver);
