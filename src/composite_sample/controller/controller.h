// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_COMPOSITE_SAMPLE_CONTROLLER_CONTROLLER_H_
#define FUCHSIA_SDK_EXAMPLES_COMPOSITE_SAMPLE_CONTROLLER_CONTROLLER_H_

#include <lib/driver/component/cpp/driver_base.h>

namespace controller_driver {

class ControllerDriver : public fdf::DriverBase {
 public:
  ControllerDriver(fdf::DriverStartArgs start_args,
                   fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("controller_driver", std::move(start_args), std::move(driver_dispatcher)) {}
  virtual ~ControllerDriver() = default;

  zx::result<> Start() override;

 private:
  zx::result<> AddChild(std::string_view name,
                        fidl::WireClient<fuchsia_driver_framework::NodeController>& controller);

  fidl::WireClient<fuchsia_driver_framework::Node> node_;
  fidl::WireClient<fuchsia_driver_framework::NodeController> child_one_;
  fidl::WireClient<fuchsia_driver_framework::NodeController> child_two_;
};

}  // namespace controller_driver

#endif  // FUCHSIA_SDK_EXAMPLES_COMPOSITE_SAMPLE_CONTROLLER_CONTROLLER_H_
