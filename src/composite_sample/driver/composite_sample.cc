// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "composite_sample.h"

#include <lib/driver/component/cpp/driver_export.h>

namespace composite_sample {

zx::result<> CompositeSampleDriver::Start() {
  FDF_SLOG(INFO, "Hello from the composite driver!");
  return zx::ok();
}

}  // namespace composite_sample

FUCHSIA_DRIVER_EXPORT(composite_sample::CompositeSampleDriver);
