// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_COMPOSITE_SAMPLE_DRIVER_COMPOSITE_SAMPLE_H_
#define FUCHSIA_SDK_EXAMPLES_COMPOSITE_SAMPLE_DRIVER_COMPOSITE_SAMPLE_H_

#include <lib/driver/component/cpp/driver_base.h>

namespace composite_sample {

class CompositeSampleDriver : public fdf::DriverBase {
 public:
  CompositeSampleDriver(fdf::DriverStartArgs start_args,
                        fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("composite-sample", std::move(start_args), std::move(driver_dispatcher)) {}

  virtual ~CompositeSampleDriver() = default;

  zx::result<> Start() override;
};

}  // namespace composite_sample

#endif  // FUCHSIA_SDK_EXAMPLES_COMPOSITE_SAMPLE_DRIVER_COMPOSITE_SAMPLE_H_
