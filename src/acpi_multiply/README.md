# ACPI Multiply Sample

This sample project contains a driver component name `acpi_multiply` for a virtual "multiplier"
device described the ACPI hardware ID `FDFS0001`.
The driver interacts with the device hardware using the `fuchsia.hardware.acpi` protocol,
and exposes a custom FIDL protocol (`examples.acpi.multiply`) for other components to
consume.

The `acpi_controller` driver component emulates an ACPI controller, creating the child device
nodes to the mutiply device and responding to requests for device resources such as MMIO and
interrupts.

## Building

To build the `acpi_multiply` driver and related components, run the following commands:

```
tools/bazel build --config=fuchsia_x64 //src/acpi_multiply/controller:pkg
tools/bazel build --config=fuchsia_x64 //src/acpi_multiply/driver:pkg
```

## Running

Use the following commands to load the driver components on a target device:

1.  Add a test node for the `acpi_controller` driver:

    ```
    tools/ffx driver test-node add controller-node \
      examples.driver.test.property=acpi-multiply-controller
    ```

1.  Load the `acpi_controller` driver component to create a virtual ACPI device node:

    ```
    tools/bazel run //src/acpi_multiply/controller:pkg.component
    ```

1.  Load the `acpi_multiply` driver component to bind to the device node and begin
    semding requests to perform multipy operations:

    ```
    tools/bazel run //src/acpi_multiply/driver:pkg.component
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --tags acpi-multiply --tags acpi-multiply-controller
    ```

### Driver client

After the drivers are loaded, use the following commands to run the multiplier client:

1.  Load the `multiply_client` component:

    ```
    tools/bazel run //src/acpi_multiply/client:pkg.component
    ```

1.  Open the device log viewer:

    ```
    tools/ffx log --filter multiply_client
    ```

You should see the client respond with an incrementing temperature value on each run:

```
[ffx-laboratory:multiply_client][I] Multiply(4294967295, 9): Invalid result: Overflow
[ffx-laboratory:multiply_client][I] Multiply(2, 9) = 18
```

## Source layout

*   `controller/` — Source code of the `acpi_controller` driver component.
*   `driver/` — Source code of the `acpi_multiply` driver component.
*   `fidl/` — FIDL library definition for `examples.acpi.multiply`.
*   `lib/` — Common device registers shared between the controller and driver components.
