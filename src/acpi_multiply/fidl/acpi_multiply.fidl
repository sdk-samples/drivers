// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

library examples.acpi.multiply;

using zx;

/// Protocol for interacting with AcpiMultiply.
protocol Device {
    /// Given two numbers to multiply, returns
    /// the value and whether or not multiplication overflowed.
    Multiply(struct {
        a uint32;
        b uint32;
    }) -> (struct {
        result uint32;
        overflowed bool;
    }) error zx.status;
};

/// The Service enables the driver framework to offer the Device protocol to
/// other components.
service Service {
  device client_end:Device;
};
