// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_CONTROLLER_ACPI_CONTROLLER_H_
#define FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_CONTROLLER_ACPI_CONTROLLER_H_

#include <lib/driver/component/cpp/driver_export.h>

#include "acpi_server.h"

namespace acpi_multiply {

// Sample driver that implements a `fuchsia.hardware.acpi` bus controller and emulates the
// following ACPI protocol behaviors for the ACPI multiple driver:
// 1. Child driver writes two values to multiply to register offsets 0x0 and 0x04
// 2. Child driver calls EvaluateObject("_MUL") to perform the multiplication, and waits
// for an interrupt.
// 3. This driver performs the multiplication, writes the result (64-bit) to 0x08
// and triggers an interrupt.
class AcpiMultiplyController : public fdf::DriverBase {
 public:
  AcpiMultiplyController(fdf::DriverStartArgs start_args,
                         fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("acpi-multiply-controller", std::move(start_args),
                        std::move(driver_dispatcher)) {}

  virtual ~AcpiMultiplyController() = default;

  zx::result<> Start() override;

 private:
  zx::result<> InitializeServer();
  zx::result<> AddChild();

  fidl::WireSyncClient<fuchsia_driver_framework::Node> node_;
  fidl::WireSyncClient<fuchsia_driver_framework::NodeController> controller_;

  std::shared_ptr<AcpiDeviceServer> server_;
};

}  // namespace acpi_multiply

#endif  // FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_CONTROLLER_ACPI_CONTROLLER_H_
