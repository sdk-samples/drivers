// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_LIB_INCLUDE_REGISTERS_H_
#define FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_LIB_INCLUDE_REGISTERS_H_

#include <hwreg/bitfields.h>

namespace acpi_multiply {

constexpr uint32_t kMultiplyAOffset = 0x00;
constexpr uint32_t kMultiplyBOffset = 0x04;
constexpr uint32_t kMultiplyResultOffset = 0x08;
constexpr uint32_t kMultiplyStatusOffset = 0x0c;

class MultiplyArgumentReg : public hwreg::RegisterBase<MultiplyArgumentReg, uint32_t> {
 public:
  DEF_FIELD(31, 0, operand);

  static auto Get(bool first) {
    return hwreg::RegisterAddr<MultiplyArgumentReg>(first ? kMultiplyAOffset : kMultiplyBOffset);
  }
};

class MultiplyResultReg : public hwreg::RegisterBase<MultiplyResultReg, uint32_t> {
 public:
  DEF_FIELD(31, 0, result);

  static auto Get() { return hwreg::RegisterAddr<MultiplyResultReg>(kMultiplyResultOffset); }
};

class MultiplyStatusReg : public hwreg::RegisterBase<MultiplyStatusReg, uint32_t> {
 public:
  DEF_BIT(1, overflow);
  DEF_BIT(0, finished);

  static auto Get() { return hwreg::RegisterAddr<MultiplyStatusReg>(kMultiplyStatusOffset); }
};

}  // namespace acpi_multiply

#endif  // FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_LIB_INCLUDE_REGISTERS_H_
