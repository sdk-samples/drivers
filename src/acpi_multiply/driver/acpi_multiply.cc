// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "acpi_multiply.h"

#include <fidl/fuchsia.hardware.acpi/cpp/wire.h>
#include <lib/async/cpp/task.h>
#include <lib/driver/component/cpp/driver_export.h>

#include "multiply_server.h"

namespace acpi_multiply {

zx::result<> AcpiMultiplyDriver::Start() {
  // Connect to the ACPI protocol exposed by the parent driver.
  zx::result client_endpoint =
      incoming()->Connect<fuchsia_hardware_acpi::Service::Device>("default");
  if (client_endpoint.is_error()) {
    FDF_SLOG(ERROR, "Failed to connect to ACPI.", KV("status", client_endpoint.status_string()));
    return client_endpoint.take_error();
  }
  auto acpi_client = fidl::WireSyncClient(std::move(*client_endpoint));

  // Initialize ACPI hardware resources
  multiplier_ = std::make_shared<AcpiMultiplier>(dispatcher(), std::move(acpi_client));
  auto init_result = multiplier_->SetupMmioAndInterrupts();
  if (init_result.is_error()) {
    FDF_SLOG(ERROR, "Failed to initialize ACPI resources.",
             KV("status", init_result.status_string()));
    return init_result.take_error();
  }
  FDF_LOG(INFO, "Hardware resources initialized.");

  // Serve the examples.acpi.multiply/Device protocol to clients through the
  // examples.acpi.multiply/Service wrapper.
  examples_acpi_multiply::Service::InstanceHandler handler({
      .device = fit::bind_member<&AcpiMultiplyDriver::Serve>(this),
  });

  auto result = outgoing()->AddService<examples_acpi_multiply::Service>(std::move(handler));
  if (result.is_error()) {
    FDF_SLOG(ERROR, "Failed to add service.", KV("status", result.status_string()));
    return result.take_error();
  }

  if (zx::result result = ExportToDevfs(); result.is_error()) {
    FDF_SLOG(ERROR, "Failed to export to devfs.", KV("status", result.status_string()));
  }
  return zx::ok();
}

zx::result<> AcpiMultiplyDriver::ExportToDevfs() {
  // Create a node for devfs.
  fidl::Arena arena;
  zx::result connector = devfs_connector_.Bind(dispatcher());
  if (connector.is_error()) {
    return connector.take_error();
  }

  auto devfs = fuchsia_driver_framework::wire::DevfsAddArgs::Builder(arena).connector(
      std::move(connector.value()));

  auto args = fuchsia_driver_framework::wire::NodeAddArgs::Builder(arena)
                  .name(arena, name())
                  .devfs_args(devfs.Build())
                  .Build();

  // Create endpoints of the `NodeController` for the node.
  zx::result controller_endpoints =
      fidl::CreateEndpoints<fuchsia_driver_framework::NodeController>();
  ZX_ASSERT_MSG(controller_endpoints.is_ok(), "Failed: %s", controller_endpoints.status_string());

  zx::result node_endpoints = fidl::CreateEndpoints<fuchsia_driver_framework::Node>();
  ZX_ASSERT_MSG(node_endpoints.is_ok(), "Failed: %s", node_endpoints.status_string());

  fidl::WireResult result = fidl::WireCall(node())->AddChild(
      args, std::move(controller_endpoints->server), std::move(node_endpoints->server));
  if (!result.ok()) {
    FDF_SLOG(ERROR, "Failed to add child", KV("status", result.status_string()));
    return zx::error(result.status());
  }
  controller_.Bind(std::move(controller_endpoints->client));
  node_.Bind(std::move(node_endpoints->client));
  return zx::ok();
}

void AcpiMultiplyDriver::Serve(fidl::ServerEnd<examples_acpi_multiply::Device> request) {
  AcpiMultiplyServer::BindDeviceClient(dispatcher(), multiplier_, std::move(request));
}

}  // namespace acpi_multiply

FUCHSIA_DRIVER_EXPORT(acpi_multiply::AcpiMultiplyDriver);
