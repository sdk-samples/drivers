// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_ACPI_MULTIPLY_H_
#define FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_ACPI_MULTIPLY_H_

#include <fidl/examples.acpi.multiply/cpp/wire.h>
#include <lib/driver/component/cpp/driver_base.h>
#include <lib/driver/devfs/cpp/connector.h>

#include "multiplier.h"

namespace acpi_multiply {

// Sample driver for a virtual "multiplier" device described using ACPI.
class AcpiMultiplyDriver : public fdf::DriverBase {
 public:
  AcpiMultiplyDriver(fdf::DriverStartArgs start_args,
                     fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : fdf::DriverBase("acpi-multiply", std::move(start_args), std::move(driver_dispatcher)),
        devfs_connector_(fit::bind_member<&AcpiMultiplyDriver::Serve>(this)) {}

  virtual ~AcpiMultiplyDriver() = default;

  zx::result<> Start() override;

 private:
  zx::result<> ExportToDevfs();
  void Serve(fidl::ServerEnd<examples_acpi_multiply::Device> request);

  driver_devfs::Connector<examples_acpi_multiply::Device> devfs_connector_;
  fidl::WireSyncClient<fuchsia_driver_framework::Node> node_;
  fidl::WireSyncClient<fuchsia_driver_framework::NodeController> controller_;

  std::shared_ptr<AcpiMultiplier> multiplier_;
};

}  // namespace acpi_multiply

#endif  // FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_ACPI_MULTIPLY_H_
