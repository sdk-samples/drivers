// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_MULTIPLY_SERVER_H_
#define FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_MULTIPLY_SERVER_H_

#include <fidl/examples.acpi.multiply/cpp/wire.h>
#include <lib/driver/logging/cpp/structured_logger.h>

#include "multiplier.h"

namespace acpi_multiply {

// FIDL server implementation for the `examples.acpi.multiply/Device` protocol
class AcpiMultiplyServer : public fidl::WireServer<examples_acpi_multiply::Device> {
 public:
  AcpiMultiplyServer(std::weak_ptr<AcpiMultiplier> multiplier) : multiplier_(multiplier) {}

  static fidl::ServerBindingRef<examples_acpi_multiply::Device> BindDeviceClient(
      async_dispatcher_t* dispatcher, std::weak_ptr<AcpiMultiplier> multiplier,
      fidl::ServerEnd<examples_acpi_multiply::Device> request);

  void OnUnbound(fidl::UnbindInfo info, fidl::ServerEnd<examples_acpi_multiply::Device> server_end);

  // fidl::WireServer<examples_acpi_multiply::Device>

  void Multiply(MultiplyRequestView request, MultiplyCompleter::Sync& completer) override;

 private:
  std::weak_ptr<AcpiMultiplier> multiplier_;
};

}  // namespace acpi_multiply

#endif  // FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_MULTIPLY_SERVER_H_
