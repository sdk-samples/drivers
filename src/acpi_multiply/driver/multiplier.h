// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_MULTIPLIER_H_
#define FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_MULTIPLIER_H_

#include <fidl/fuchsia.hardware.acpi/cpp/wire.h>
#include <lib/async/cpp/irq.h>
#include <lib/mmio/mmio-buffer.h>
#include <lib/zx/interrupt.h>

#include <list>

namespace acpi_multiply {

// Invokes ACPI methods using a fuchsia.hardware.acpi client.
class AcpiMultiplier {
 public:
  explicit AcpiMultiplier(async_dispatcher_t* dispatcher,
                          fidl::WireSyncClient<fuchsia_hardware_acpi::Device> acpi)
      : dispatcher_(dispatcher), acpi_(std::move(acpi)) {}

  zx::result<> SetupMmioAndInterrupts();

  // Structure to return operation results to FIDL callers.
  struct MultiplyResult {
    uint32_t value;
    uint32_t overflow;
  };

  // Structure to receive operation requests from FIDL callers.
  struct Operation {
    // Operands to the multiply.
    uint32_t a;
    uint32_t b;
    // Operation callback.
    fit::callback<void(zx::result<MultiplyResult>)> callback;
  };

  void QueueMultiplyOperation(Operation operation);

 private:
  void HandleIrq(async_dispatcher_t* dispatcher, async::IrqBase* irq, zx_status_t status,
                 const zx_packet_interrupt_t* interrupt);

  void DoMultiply(Operation operation);

  async_dispatcher_t* const dispatcher_;

  std::optional<fdf::MmioBuffer> mmio_;
  zx::interrupt irq_;
  async::IrqMethod<AcpiMultiplier, &AcpiMultiplier::HandleIrq> irq_method_{this};
  fidl::WireSyncClient<fuchsia_hardware_acpi::Device> acpi_;

  std::optional<Operation> current_op_;
  std::list<Operation> operation_queue_;
};

}  // namespace acpi_multiply

#endif  // FUCHSIA_SDK_EXAMPLES_ACPI_MULTIPLY_DRIVER_MULTIPLIER_H_
