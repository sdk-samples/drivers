// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "multiply_server.h"

namespace acpi_multiply {

// Static
// Handle incoming connection requests from FIDL clients
fidl::ServerBindingRef<examples_acpi_multiply::Device> AcpiMultiplyServer::BindDeviceClient(
    async_dispatcher_t* dispatcher, std::weak_ptr<AcpiMultiplier> multiplier,
    fidl::ServerEnd<examples_acpi_multiply::Device> request) {
  // Bind each connection request to a unique FIDL server instance
  auto server_impl = std::make_unique<AcpiMultiplyServer>(multiplier);
  return fidl::BindServer(dispatcher, std::move(request), std::move(server_impl),
                          std::mem_fn(&AcpiMultiplyServer::OnUnbound));
}

// This method is called when a server connection is torn down.
void AcpiMultiplyServer::OnUnbound(fidl::UnbindInfo info,
                                   fidl::ServerEnd<examples_acpi_multiply::Device> server_end) {
  if (info.is_peer_closed()) {
    FDF_LOG(DEBUG, "Client disconnected");
  } else if (!info.is_user_initiated()) {
    FDF_LOG(ERROR, "Client connection unbound: %s", info.status_string());
  }
}

// Protocol method in `examples.acpi.multiply` to execute a multiply operation
void AcpiMultiplyServer::Multiply(MultiplyRequestView request, MultiplyCompleter::Sync& completer) {
  auto multiplier = multiplier_.lock();
  if (!multiplier) {
    FDF_LOG(ERROR, "Unable to access ACPI resources.");
    completer.ReplyError(ZX_ERR_BAD_STATE);
    return;
  }

  multiplier->QueueMultiplyOperation(AcpiMultiplier::Operation{
      .a = request->a,
      .b = request->b,
      .callback =
          [completer =
               completer.ToAsync()](zx::result<AcpiMultiplier::MultiplyResult> status) mutable {
            if (status.is_error()) {
              completer.ReplyError(status.error_value());
              return;
            }

            AcpiMultiplier::MultiplyResult result = status.value();
            completer.ReplySuccess(result.value, result.overflow);
          },
  });
}

}  // namespace acpi_multiply
