#!/bin/bash

# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


# Run this from the root of the repo with `scripts/smoke_test.sh`
set -e -o pipefail

emu_name=_smoke_test

if [[ -z "$HOME" ]]; then
  export HOME=$(pwd)
fi

failure() {
    echo >&2 "ERROR: $0 line $1"
}

print_and_run() {
  echo
  echo "*** $@"
  "$@"
}

wait_for_emu_rcs() {
  local i=0
  while [[ $i -lt 120 ]] &&
      [[ "$(tools/ffx target list 2>&1 | grep $emu_name | awk '{print $6;}')" != "Y" ]]; do
    echo -n .
    sleep 0.5
    i+=1
  done
  echo
}

main() {
  trap 'failure $LINENO' ERR
  local keep_emu=0
  local skip_clean=0
  local only_kvm=0
  while [[ $# -gt 0 ]]; do
    if [[ "$1" == "--keep" ]]; then
      keep_emu=1
    elif [[ "$1" == "--no-clean" ]]; then
      skip_clean=1
    elif [[ "$1" == "--only-kvm" ]]; then
      only_kvm=1
    else
      echo >&2 "Invalid argument: $1"
      echo >&2 "Syntax: $0 [--keep] [--no-clean] [--only-kvm]"
      return 1
    fi
    shift
  done

  print_and_run scripts/bootstrap.sh

  echo "Build starting!"

  if [[ $skip_clean -eq 0 ]]; then
    # clear previously fetched dependencies to ensure a clean environment
    print_and_run tools/bazel clean --expunge
  fi

  # Download the SDK toolchain
  print_and_run tools/bazel build @fuchsia_sdk//:fuchsia_toolchain_sdk
  # build all samples in this repository
  print_and_run tools/bazel build src:fuchsia_samples

  print_and_run tools/ffx doctor --restart-daemon
  print_and_run tools/ffx sdk version

  current_target="$(tools/ffx target default get)"

  repo_name="workstation-packages"
  pb_name="core.x64-dfv2"

  # fetch an emulator image of workstation and start an emulator
  print_and_run tools/ffx product-bundle get "$pb_name" --repository "$repo_name" --force-repo
  stop_emu

  # If KVM is available, then run emulator tests with default flags that will use KVM
  if [[ "$(uname -s)" != "Linux" || -w /dev/kvm ]]; then
    run_emulator_tests 0
  elif [[ ${only_kvm} -eq 1 ]]; then
    echo "Virtualization not available, but --only-kvm selected so no tests would run"
    exit 1
  fi
  # Run the emulator tests again with extra flags to support disabled KVM
  if [[ ${only_kvm} -eq 0 ]]; then
    stop_emu
    run_emulator_tests 1
  fi
}

run_emulator_tests() {
  local disable_kvm="$1"
  local emu_options=""
  # If KVM is not available, need to fall back to an alternative emulator that works fxbug.dev/113237
  # otherwise it fails to start up properly, and ffx emu does not handle this yet.
  if [[ ${disable_kvm} -eq 1 ]]; then
    echo "Running emulator tests with KVM disabled"
    emu_options="--engine qemu --startup-timeout 720 --accel none --device qemu-x64-emu-min"
  else
    echo "Running emulator tests with default KVM"
  fi
  print_and_run tools/ffx emu start "$pb_name" --headless --name $emu_name $emu_options

  if [[ $keep_emu -eq 0 ]]; then
    trap "stop_emu" EXIT
  fi

  wait_for_emu_rcs

  # CQ actually uses mawk and not awk or gawk, so does not support symbols in search string
  check_output() {
    if ! awk -v SEARCH="$@" '$0 ~ SEARCH{rc=1; print "Found match: "$0}{print $0}; END{exit !rc}'; then
      echo "Failed to find expected string: $@"
      exit 1
    fi
  }

  # start the package server
  print_and_run tools/ffx repository server start

  # enable experimental ffx driver plugin
  print_and_run tools/ffx config set driver true

  print_and_run tools/ffx target default set $emu_name

  # register the package repository with on-demand prebuilt packages
  print_and_run tools/ffx target repository register -r $repo_name --alias fuchsia.com --alias chromium.org
  print_and_run tools/ffx repository list

  # register a driver and verify that it registered succesfully
  print_and_run tools/bazel run //src/qemu_edu/drivers:pkg.component | check_output "Node 'dev.sys.platform.pt.PCI0.bus"

  # wait for driver to load
  sleep 1
  print_and_run tools/ffx driver list --loaded | check_output "qemu_edu"

  # run tests on the emulator
  print_and_run tools/bazel test src/qemu_edu/tests:pkg
  print_and_run tools/bazel run //src/qemu_edu/tools:pkg.eductl_tool -- live
  # TODO: fxbug.dev/113254     | check_output "Liveness check passed"
  print_and_run tools/bazel run //src/qemu_edu/tools:pkg.eductl_tool -- fact 12
  # TODO: fxbug.dev/113254     | check_output "479001600"

  # other run commands in https://fuchsia.dev/fuchsia-src/get-started/sdk/get-started-with-driver
  print_and_run tools/bazel run //src/qemu_edu/drivers:pkg.component | check_output "Success"
  print_and_run tools/ffx component show qemu_edu.cm | check_output "Moniker:  /bootstrap/full-pkg-drivers:dev.sys.platform.pt.PCI0.bus."
  print_and_run tools/ffx driver list-devices | check_output "dev.sys.platform.pt.PCI0.bus."
  # The PCI id changes depending if /dev/kvm is available
  if [[ ${disable_kvm} -eq 1 ]]; then
    print_and_run tools/ffx driver list-devices dev.sys.platform.pt.PCI0.bus.00_05.0_ --verbose | check_output "Name     : 0_"
  else
    print_and_run tools/ffx driver list-devices dev.sys.platform.pt.PCI0.bus.00_06.0_ --verbose | check_output "Name     : 0_"
  fi
  print_and_run tools/ffx driver list-hosts | check_output "meta/qemu_edu.cm"
  print_and_run tools/ffx driver lspci | check_output "Host bridge:"
  # check debugging tools work
  test_zxdb() { echo quit | tools/ffx debug connect; }
  print_and_run test_zxdb | check_output "Connected successfully"
  print_and_run test_fidlcat
  print_and_run tools/ffx target snapshot -d .
  print_and_run unzip -v snapshot.zip
  # CQ uses a non-HOME directory for .ssh so pass in the private key directly for all cases
  print_and_run tools/fssh -sshconfig scripts/sshconfig.local -private-key "${HOME}/.ssh/fuchsia_ed25519" uname -a | check_output "Zircon"

  echo "Success!"
}

test_fidlcat() {
  local bazel_out
  bazel_out="$(tools/bazel info output_path)"
  timeout 10s tools/ffx debug fidl --remote-name qemu_edu.cm --fidl-ir-path "$bazel_out" || if [ "$?" != "124" ]; then /bin/false; fi
}

stop_emu() {
  # stop the emulator
  if tools/ffx emu show $emu_name >/dev/null 2>&1; then
    echo "Stopping emulator"
    tools/ffx emu stop $emu_name || true
  fi
  if [[ -n "${current_target}" ]]; then
    tools/ffx target default set "${current_target}" || true
  else
    tools/ffx target default unset || true
  fi

  return 0
}

time main "$@"
